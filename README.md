# allMs
这是一个用于消息队列的高级抽象，使用它，我们可以在不更换任何代码的时候切换消息队列，我们知道，kafka是没有遵循jms规范的，如果我们先前使用的是activemq或者其它的mq，那么我们切换成为kafka是很困难的，所以有了allMs，我们可以在不需要改动任何代码的情况下切换任何我们需要的消息队列，改配置就行了