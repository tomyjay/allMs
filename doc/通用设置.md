```java
Configuration configuration = new Configuration();
configuration.add("mq","activemq");
configuration.add("mq","kafka");
```

通过指定mq参数可以指定消息队列的类型

```java
AllMsConnectionFactory factory = new AllMsConnectionFactory(configuration);
```

或者不设置参数也可以通过代码显示传递

```java
AllMsConnectionFactory factory = new AllMsConnectionFactory(configuration,MQ.KAFKA);
```
