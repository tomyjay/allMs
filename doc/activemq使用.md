参数说明

|参数|是否必传|默认值|说明|
|-|-|-|-|
|username|是|无|activemq的用户名|
|password|是|无|activemq的密码|
|brokerURL|是|无|activemq的连接地址|
|type|否|0|0 代表着使用列队，1代表使用订阅主题|
|transaction|否|false|``connection.createSession()``中的第一个参数，是否要事务|
|acknowledgeMode|否|1|``connection.createSession()``中的第2个参数，消息确认模式，如果transaction为true，则会忽略第二个参数|


一般无事务要求，transaction和acknowledgeMode可以不传

介绍一下acknowledgeMode的选项

|选项|说明|
|-|-|
|1|自动确认，哪怕consumer出现异常，也被当作处理成功|
|2|为客户端确认。客户端接收到消息后，必须调用javax.jms.Message的acknowledge方法。jms服务器才会当作发送成功，并删除消息|
|3|允许副本的确认模式。一旦接consumer应用程序的方法调用从处理消息处返回，会话对象就会确认消息的接收；而且允许重复确认|