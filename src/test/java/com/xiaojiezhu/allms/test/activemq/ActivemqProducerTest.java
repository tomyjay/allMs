package com.xiaojiezhu.allms.test.activemq;

import com.xiaojiezhu.allms.AllMsConnectionFactory;
import com.xiaojiezhu.allms.Configuration;
import com.xiaojiezhu.allms.MQ;
import com.xiaojiezhu.allms.ms.Connection;
import com.xiaojiezhu.allms.ms.message.Message;
import com.xiaojiezhu.allms.ms.producer.Producer;

/**
 * @author zxj<br>
 * 时间 2018/3/19 18:01
 * 说明 ...
 */
public class ActivemqProducerTest {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.add("username","admin");
        configuration.add("password","Yba*&@#LLba167");
        configuration.add("brokerURL","failover:(tcp://127.0.0.1:61616)");
        AllMsConnectionFactory allMsConnectionFactory = new AllMsConnectionFactory(configuration, MQ.ACTIVEMQ);

        Connection connection = allMsConnectionFactory.getConnection();
        Producer producer = connection.createProducer("abc");

        for(int i = 0 ; i < 100 ; i ++){
            long s = System.currentTimeMillis();
            producer.sendMessage(new Message("你好啊" + i));
            long e = System.currentTimeMillis();
            System.out.println(e - s);
        }
        System.out.println("完成");
    }
}
