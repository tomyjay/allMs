package com.xiaojiezhu.allms.test.activemq;

import com.xiaojiezhu.allms.AllMsConnectionFactory;
import com.xiaojiezhu.allms.Configuration;
import com.xiaojiezhu.allms.MQ;
import com.xiaojiezhu.allms.ms.Connection;
import com.xiaojiezhu.allms.ms.consumer.Consumer;
import com.xiaojiezhu.allms.ms.consumer.MessageListener;
import com.xiaojiezhu.allms.ms.message.Message;

/**
 * @author zxj<br>
 * 时间 2018/3/19 18:04
 * 说明 ...
 */
public class ActivemqConsumerTest {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.add("username","admin");
        configuration.add("password","121");
        configuration.add("brokerURL","failover:(tcp://127.0.0.1:61616)");
        AllMsConnectionFactory allMsConnectionFactory = new AllMsConnectionFactory(configuration, MQ.ACTIVEMQ);

        Connection connection = allMsConnectionFactory.getConnection();
        Consumer consumer = connection.createConsumer("abc");
        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                System.out.println(message);
            }
        });
    }
}
