package com.xiaojiezhu.allms.test.kafka;

import com.xiaojiezhu.allms.AllMsConnectionFactory;
import com.xiaojiezhu.allms.Configuration;
import com.xiaojiezhu.allms.MQ;
import com.xiaojiezhu.allms.ms.Connection;
import com.xiaojiezhu.allms.ms.message.KeyValueMessage;
import com.xiaojiezhu.allms.ms.message.Message;
import com.xiaojiezhu.allms.ms.producer.Producer;

/**
 * @author zxj<br>
 * 时间 2018/3/19 16:11
 * 说明 ...
 */
public class KafkaProducerTest {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.add("bootstrap.servers","127.0.0.1:9092");
        configuration.add("acks","all");
        configuration.add("retries",0);
        configuration.add("batch.size", 16384);
        configuration.add("linger.ms", 1);
        configuration.add("buffer.memory", 33554432);
        configuration.add("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        configuration.add("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");


        AllMsConnectionFactory allMsConnectionFactory = new AllMsConnectionFactory(configuration, MQ.KAFKA);
        Connection connection = allMsConnectionFactory.getConnection();
        Producer producer = connection.createProducer("abc");

        for(int i = 0 ; i < 100 ; i ++){
           producer.sendMessage(new KeyValueMessage("key:" + i , "value:" + i));
        }
    }
}
