package com.xiaojiezhu.allms.test.kafka;

import com.xiaojiezhu.allms.AllMsConnectionFactory;
import com.xiaojiezhu.allms.Configuration;
import com.xiaojiezhu.allms.MQ;
import com.xiaojiezhu.allms.ms.Connection;
import com.xiaojiezhu.allms.ms.consumer.Consumer;
import com.xiaojiezhu.allms.ms.consumer.MessageListener;
import com.xiaojiezhu.allms.ms.message.Message;

/**
 * @author zxj<br>
 * 时间 2018/3/19 16:25
 * 说明 ...
 */
public class KafkaConsumerTest {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.add("bootstrap.servers","127.0.0.1:9092");
        configuration.add("group.id","testGroup");
        configuration.add("enable.auto.commit", "true");
        configuration.add("auto.commit.interval.ms", "1000");
        configuration.add("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        configuration.add("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        AllMsConnectionFactory allMsConnectionFactory = new AllMsConnectionFactory(configuration, MQ.KAFKA);
        Connection connection = allMsConnectionFactory.getConnection();
        Consumer consumer = connection.createConsumer("abc");
        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                System.out.println(message);
            }
        });
    }
}
