package com.xiaojiezhu.allms.ms.creator;

import com.xiaojiezhu.allms.Configuration;
import com.xiaojiezhu.allms.ms.Connection;
import com.xiaojiezhu.allms.ms.KafkaConnection;
import com.xiaojiezhu.allms.util.Asserts;

/**
 * @author zxj<br>
 * 时间 2018/3/19 11:39
 * 说明 创建kafka的connection
 */
public class KafkaConnectionCreator implements ConnectionCreator {
    private Configuration configuration;


    @Override
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Connection create() {
        Asserts.notNull(configuration,"configuration is not set");
        return new KafkaConnection(this.configuration);
    }
}
