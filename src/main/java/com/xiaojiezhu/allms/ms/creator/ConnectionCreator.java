package com.xiaojiezhu.allms.ms.creator;

import com.xiaojiezhu.allms.Configuration;
import com.xiaojiezhu.allms.exception.CreateConnectionFailException;
import com.xiaojiezhu.allms.ms.Connection;

/**
 * @author zxj<br>
 * 时间 2018/3/19 11:30
 * 说明 ...
 */
public interface ConnectionCreator {

    void setConfiguration(Configuration configuration);

    Connection create() throws CreateConnectionFailException;
}
