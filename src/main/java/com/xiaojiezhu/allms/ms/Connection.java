package com.xiaojiezhu.allms.ms;

import com.xiaojiezhu.allms.exception.CreateConnectionFailException;
import com.xiaojiezhu.allms.ms.consumer.Consumer;
import com.xiaojiezhu.allms.ms.producer.Producer;

/**
 * @author zxj<br>
 * 时间 2018/3/19 11:26
 * 说明 ...
 */
public interface Connection{



    /**
     * 创建consumer
     * @return
     */
    Consumer createConsumer(String topic) throws CreateConnectionFailException;

    /**
     * 创建producer
     * @return
     */
    Producer createProducer(String topic) throws CreateConnectionFailException;


}
