package com.xiaojiezhu.allms.ms.consumer;

import java.io.Closeable;

/**
 * @author zxj<br>
 * 时间 2018/3/19 11:27
 * 说明 ...
 */
public interface Consumer extends Closeable {

    /**
     * set a listener
     * @param messageListener
     */
    void setMessageListener(MessageListener messageListener);



}
