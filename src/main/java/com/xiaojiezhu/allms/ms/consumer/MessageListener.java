package com.xiaojiezhu.allms.ms.consumer;

import com.xiaojiezhu.allms.ms.message.Message;

/**
 * @author zxj<br>
 * 时间 2018/3/19 14:52
 * 说明 ...
 */
public interface MessageListener {


    void onMessage(Message message);


}
